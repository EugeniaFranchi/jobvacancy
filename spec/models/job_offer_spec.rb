require 'spec_helper'

describe JobOffer do
  describe 'valid?' do
    it 'should be invalid when title is blank' do
      check_validation(:title, "Title can't be blank") do
        described_class.new(location: 'a location')
      end
    end

    it 'should be valid when title is not blank' do
      job_offer = described_class.new(title: 'a title')
      expect(job_offer).to be_valid
    end

    it 'should be invalid when experience is negative' do
      experience = -2
      check_validation(:experience, 'Experience must be greater than or equal to 0') do
        described_class.new(title: 'a title', experience: experience)
      end
    end

    it 'should be invalid when experience is float' do
      experience = 2.1
      check_validation(:experience, 'Experience must be an integer') do
        described_class.new(title: 'a title', experience: experience)
      end
    end
  end

  describe 'job experience' do
    it 'job offer initialized with experience 2 should have experience 2' do
      experience = 2
      job_offer = described_class.new(title: 'a title', experience: experience)
      expect(job_offer.experience).to eq experience
    end
  end
end
